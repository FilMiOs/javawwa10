package com.ostrycharz.javawwa10.listy;

import org.junit.Test;

public class WezelTest {
    @Test
    public void dodanieWezla() {
        Wezel wezel = new Wezel(29,null);
        Wezel wezelNastepny = new Wezel(30, null);
        Wezel wezelDoDodania = new Wezel(31, null);
        wezel.nastepny = wezelNastepny;
        ListaJendokierunkowa lista = new ListaJendokierunkowa(wezel);

        lista.dodajWezel(wezelDoDodania);
        assert lista.getWezel(2) == 31;
        assert lista.getWezel(1) != 31;
        lista.usunWezel(1);
        assert lista.getWezel(1) == 31;
        assert lista.getWezel(2) == -1;

    }
}
