package com.ostrycharz.javawwa10.algorytmysortowania;


import org.junit.Assert;
import org.junit.Test;

public class SortTests {

    @Test
    public void testBubbleSort() {
        int[] tabliceNiePosortowana = {4, 2, 1, 5, 7};
        int[] tablicaPosortowana = {1, 2, 4, 5, 7};

        Assert.assertArrayEquals(tablicaPosortowana, BubbleSort.bubbleSort(tabliceNiePosortowana));
    }

    @Test
    public void testInsertSort() {
        int[] tabliceNiePosortowana = {4, 2, 1, 5, 7};
        int[] tablicaPosortowana = {1, 2, 4, 5, 7};

        Assert.assertArrayEquals(tablicaPosortowana, InsertSort.sort(tabliceNiePosortowana));
    }

    @Test
    public void testQuickSortMiddle() {
        int[] tabliceNiePosortowana = {4, 2, 1, 5, 7};
        int[] tablicaPosortowana = {1, 2, 4, 5, 7};

        Assert.assertArrayEquals(tablicaPosortowana, QuickSort.sortPivotMiddle(tabliceNiePosortowana, 0, tabliceNiePosortowana.length - 1));
    }

    @Test
    public void testQuickSortFirst() {
        int[] tabliceNiePosortowana = {4, 5, 6, 8, 7};
        int[] tablicaPosortowana = {4, 5, 6, 7, 8};

        QuickSort qs = new QuickSort();
        qs.sortPivotFirst(tabliceNiePosortowana);
        Assert.assertArrayEquals(tablicaPosortowana, tabliceNiePosortowana);
    }
    @Test
    public void czyJestWtablicy() {

        int[] tablica = {38, 27, 43, 3, 9, 82, 10};
        int poszukiwany = 3;

        for (int i = 0; i < tablica.length; i++) {
            if (poszukiwany == tablica[i]) {
                System.out.println(i);
                return;
            }
        }
        System.out.println(-1);
    }

    @Test
    public void binarySearch() {
        int[] tablica = {38, 27, 43, 3, 9, 82, 10};
        int[] posrtowana = InsertSort.sort(tablica);
        int lewy = 0;
        int szukany = 4;
        int prawy = posrtowana.length - 1;
        while (lewy <= prawy) {
            int m = (lewy + prawy) / 2;
            if (posrtowana[m] < szukany) {
                lewy = m + 1;
            } else if (posrtowana[m] > szukany) {
                prawy = m - 1;
            } else {
                System.out.println(m);
                return;
            }
        }
        System.out.println("nie ma");
    }

    @Test
    public void test() {
        for (int i = 1; i > 0; --i) {
            System.out.println(i);
        }
    }
}




