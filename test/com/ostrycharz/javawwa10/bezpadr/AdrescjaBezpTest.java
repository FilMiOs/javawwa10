package com.ostrycharz.javawwa10.bezpadr;

import org.junit.Test;

public class AdrescjaBezpTest {

    @Test
    public void dodanieWartosci() {
        AdresacjaBezp ab = new AdresacjaBezp(15);
        ab.dodajWartosc(5);
        assert ab.sprawdzCzyJestWartosc(5);
    }

    @Test
    public void usuniecieWartosci() {
        AdresacjaBezp ab = new AdresacjaBezp(15);
        ab.dodajWartosc(5);
        assert ab.sprawdzCzyJestWartosc(5);
        ab.usunWartosc(5);
        assert !ab.sprawdzCzyJestWartosc(5);
    }

}
