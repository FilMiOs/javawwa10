package com.ostrycharz.javawwa10.bst;

import org.junit.Test;

public class BstTest {


    @Test
    public void testDodajWezelDoDrzewaPustego() {
        Bst bst = new Bst(null);
        Wezel w = new Wezel(5);
        bst.dodajWezel(w);

    }

    @Test
    public void testDodajWezelDoDrzewNiePustego() {
        Bst bst = new Bst(null);
        Wezel w = new Wezel(5);
        Wezel nastepny = new Wezel(6);
        bst.dodajWezel(w);
        bst.dodajWezel(nastepny);

        System.out.println(bst.korzen.liczba);
    }
}
