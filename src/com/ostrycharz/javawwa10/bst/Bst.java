package com.ostrycharz.javawwa10.bst;

public class Bst {

    Wezel korzen = null;

    public Bst(Wezel korzen) {
        this.korzen = korzen;
    }

    public void dodajWezel(Wezel dodaj) {
        if(korzen == null) {
            korzen = dodaj;
        } else if(korzen.liczba > dodaj.liczba) {
            dodajWezel(dodaj);
        } else {
            dodajWezel(dodaj);
        }
    }

}
