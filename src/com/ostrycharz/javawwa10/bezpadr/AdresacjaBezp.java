package com.ostrycharz.javawwa10.bezpadr;

class AdresacjaBezp {

    private boolean[] adresy;

    AdresacjaBezp(int wielkosc) {
        adresy = new boolean[wielkosc];
    }
    void dodajWartosc(int i) {
        adresy[i] = true;
    }
    boolean sprawdzCzyJestWartosc(int i) {
        return adresy[i];
    }
    void usunWartosc(int i) {
        adresy[i] = false;
    }
}
