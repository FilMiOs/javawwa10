package com.ostrycharz.javawwa10.algorytmysortowania;

class InsertSort {

    static int[] sort(int[] tablica) {
        for (int  i = 1; i<tablica.length; i++) {
            int elemPor = tablica[i];
            int elemPoprz = i - 1;

            for (;elemPoprz >=0 && tablica[elemPoprz] > elemPor;elemPoprz-- ){
                tablica[elemPoprz + 1 ] = tablica[elemPoprz];
            }
            tablica[elemPoprz + 1] = elemPor;
        }
        return tablica;
    }

}