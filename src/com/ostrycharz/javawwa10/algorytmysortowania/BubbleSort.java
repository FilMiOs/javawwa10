package com.ostrycharz.javawwa10.algorytmysortowania;

class BubbleSort {
    static int[] bubbleSort(int[] tablica) {
        int n = tablica.length;
        int temp;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (tablica[j - 1] > tablica[j]) {
                    //zamiana elementów
                    temp = tablica[j - 1];
                    tablica[j - 1] = tablica[j];
                    tablica[j] = temp;
                }
            }
        }
        return tablica;
    }
}

