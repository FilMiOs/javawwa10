package com.ostrycharz.javawwa10.algorytmysortowania;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class QuickSort {

    static int[] sortPivotMiddle(int[] tab, int lewy, int prawy) {
        if(prawy <= lewy) return tab;

        int i = lewy - 1, j = prawy + 1,
                pivot = tab[(lewy+prawy)/2]; //wybieramy punkt odniesienia


        List<String> listTekst = new ArrayList<>();
        listTekst.add("Tekst");
        listTekst.get(0);

        while(true)
        {
            //szukam elementu wiekszego lub rownego piwot stojacego
            //po prawej stronie wartosci pivot
            while(pivot>tab[++i]);
            //szukam elementu mniejszego lub rownego pivot stojacego
            //po lewej stronie wartosci pivot
            while(pivot<tab[--j]);
            //jesli liczniki sie nie minely to zamień elementy ze soba
            //stojace po niewlasciwej stronie elementu pivot
            if( i <= j) {
                int swap = tab[i];
                tab[i] = tab[j];
                tab[j] = swap;
            }
            //funkcja swap zamienia wartosciami tab[i] z tab[j]
            else
                break;
        }
        if(j > lewy)
            sortPivotMiddle(tab, lewy, j);
        if(i < prawy)
            sortPivotMiddle(tab, i, prawy);
        return tab;
    }

        /**
         * Quick sortPivotFirst the given array in ascending order
         *
         * @param array
         */
        public void sortPivotFirst(int[] array) {
            sortPivotFirst(array, 0, array.length - 1);
        }

        /**
         * Quick sortPivotFirst the given array starting from index
         * {@code l} to {@code r}
         *
         * Uses the first element in the array as the pivot
         *
         * @param array
         * @param left
         * @param right
         */
        private void sortPivotFirst(int[] array, int left, int right) {
            if (left < right) {
                // select pivot element (left-most)
                int pivot = array[left];
                // partition and shuffle around pivot
                int i = left;
                int j = right;
                while (i < j) {
                    // move right to avoid pivot element
                    i += 1;
                    // scan right: find elements greater than pivot
                    while (i <= right && array[i] < pivot) {
                        i += 1;
                    }
                    // scan left: find elements smaller than pivot
                    while (j >= left && array[j] > pivot) {
                        j -= 1;
                    }
                    if (i <= right && i < j) {
                        // swap around pivot
                        swap(array, i, j);
                    }
                }
                // put pivot in correct place
                swap(array, left, j);
                // sortPivotFirst partitions
                sortPivotFirst(array, left, j - 1);
                sortPivotFirst(array, j + 1, right);
            }
        }

        /**
         * Swap elements at indexes {@code i} and {@code j}
         * in the give array
         *
         * @param array
         * @param i
         * @param j
         */
        private void swap(int[] array, int i, int j) {
            if (i >= 0 && j >= 0 && i < array.length && j < array.length) {
                int tmp = array[i];
                array[i] = array[j];
                array[j] = tmp;
            }
        }
}

