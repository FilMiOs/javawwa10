package com.ostrycharz.javawwa10.listy;

public class ListaJendokierunkowa {
    Wezel root = null;
    public ListaJendokierunkowa(Wezel root) {
        this.root = root;
    }

    public void dodajWezel(Wezel wezel) {
        if (root==null) {
            root = wezel;
        } else {
            Wezel next = root.nastepny;
            while(next.nastepny != null) {
                next = next.nastepny;
            }
            next.nastepny = wezel;
        }
    }

    public int getWezel(int index) {
        if(root==null || index < 0) {
            return -1;
        }
        Wezel next = root;
        for(int i=0;i<index ;i++) {
            if(next.nastepny == null) {
                return -1;
            }
            next = next.nastepny;
        }
        return next.liczba;
    }
    public void usunWezel(int index) {
        if(root==null || index < 0) {
            return;
        }
        Wezel current = root;
        Wezel next = root.nastepny;
        for (int i = 0; i < index - 1; i++) {
            current = next;
            next = next.nastepny;

        }
        current.nastepny = next.nastepny;

    }

}
