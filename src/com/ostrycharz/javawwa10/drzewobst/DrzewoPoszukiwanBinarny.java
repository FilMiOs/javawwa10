package com.ostrycharz.javawwa10.drzewobst;

public class DrzewoPoszukiwanBinarny {
    private Wezel root = null;   // korzeń naszego drzewa

    /* Dodawanie elementów */
    public void dodajWezel(int wstawianyKlucz) {
        if (root == null)
            root = new Wezel(wstawianyKlucz);
        else {
            Wezel actual = root;
            Wezel parent = null;
            // poszukiwanie miejsca dla elementu
            while (actual != null) {
                parent = actual;
                actual = (wstawianyKlucz < actual.key) ? actual.left : actual.right;
            }
            //Koncowe porownanie aby wiedziec gdzie wstawic
            if (parent.key > wstawianyKlucz) {
                parent.left = new Wezel(wstawianyKlucz);
                parent.left.parent = parent;
            } else {
                parent.right = new Wezel(wstawianyKlucz);
                parent.right.parent = parent;
            }
        }
    }

    /* Wyszukiwanie elementu */
    public Wezel wyszukajWezel(int key)  {
        Wezel actual = root;
        while(actual != null && actual.key != key)
            actual = (actual.key > key) ? actual.left : actual.right;
        if(actual == null)
            return null;
        return actual;
    }






    private Wezel successor(int key) throws TreeException {
        Wezel wezel = this.wyszukajWezel(key);
// Szukanie następnika gdy węzeł ma prawego potomka
        if(wezel.right != null) {
            wezel = wezel.right;
            while(wezel.left != null)
                wezel = wezel.left;
            return wezel;
        }
// Szukanie następnika gdy węzeł nie ma prawgo potomka
        else if(wezel.right == null && wezel != root && wezel != this.max(root)) {
            Wezel parent = wezel.parent;
            while(parent != root && parent.key < wezel.key)
                parent = parent.parent;
            return parent;
        }
        else
            throw new TreeException("Not Found Successor");
    }
    // Znajdowanie minimalnego klucza
    private Wezel max(Wezel wezel) {
        while(wezel.right != null)
            wezel = wezel.right;
        return wezel;
    }
    /* Usuwanie elementu */
    public Wezel remove(int key) throws TreeException {
        Wezel wezel = this.wyszukajWezel(key);
        Wezel parent = wezel.parent;
        Wezel tmp;
        if(wezel.left != null && wezel.right != null) {
            tmp = this.remove(this.successor(key).key);
            tmp.left = wezel.left;
            if(tmp.left != null)
                tmp.left.parent = tmp;
            tmp.right = wezel.right;
            if(tmp.right != null)
                tmp.right.parent = tmp;
        }
        else
            tmp = (wezel.left != null) ? wezel.left : wezel.right;
        if(tmp != null)
            wezel.parent = parent;
        if(parent == null)
            root = tmp;
        else if(parent.left == wezel)
            parent.left = tmp;
        else
            parent.right = tmp;
        return wezel;
    }
    // Wyjątki wyrzucane przez drzewo
    private class TreeException extends Throwable {
        TreeException() {
        }

        TreeException(String msg) {
            super(msg);
        }
    }
}
